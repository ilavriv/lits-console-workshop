const fs = require('fs')
const path = require('path')
const util = require('util')


const createDirAsync = util.promisify(fs.mkdir)


const t = fileName => `// ${fileName}`

module.exports = class FsProcessor{
  resolvePath(name, ...parentPaths) {
    const relPaths = parentPaths.join('/')

    return path.resolve(
      process.cwd(),
      relPaths,
      name
    )
  }

  createFile(name, ...parentPaths) {
    const filePath = this.resolvePath(name, ...parentPaths)

    if(fs.existsSync(filePath)) {
      return
    }

    fs.writeFileSync(filePath, t(name))
  }

  async createDir({ name, files = [] }, moduleName) {
    const dirPath = this.resolvePath(name, moduleName)

    if (fs.existsSync(dirPath)) {
      return
    }

    
    try {

      await createDirAsync(dirPath)

      if (!files.length) {
        return
      }
  
      
      files.forEach(fileName => {
        this.createFile(fileName, moduleName, name)
      });

    } catch (err) {
      throw err
    }
  }
}