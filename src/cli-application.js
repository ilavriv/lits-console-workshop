const ArgumentParser = require('argparse').ArgumentParser

const {
  ModuleGenerator,
  ComponentGenerator 
} = require('./generators')

class CliApplication {
  constructor() {
    this.parser = new ArgumentParser({
      name: 'My module generator',
      addHelp: true,
      version: '0.0.1',
      description: "My own module generator"
    })

    this.subparsers = this.parser.addSubparsers({
      name: 'subparsers',
      dest: 'subcommand'
    })

    
    this.componentParser = this.subparsers.addParser('component')
      
    this.componentParser.addArgument(['name'])
    this.componentParser.addArgument(['--moduleName', '-m'])

    const moduleParser = this.subparsers.addParser('module')
      
    moduleParser.addArgument(['name'])
    moduleParser.addArgument(['--config', '-c'])
  }


  parseArgs() {
    const moduleName = this.parser.parseArgs(['subcommand']);

    console.log('module', moduleName)


    const args = this.parser.parseArgs()
    
    console.log('args', args)

    

    // new ComponentGenerator(args).generate()

    // 
  }
}


module.exports = CliApplication

