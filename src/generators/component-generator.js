const path = require('path')
const fs = require('fs')
const mustache = require('mustache')

const FsProcessor = require('../utils/fs-processor')

const t = name => `${name}.jsx`

const TEMPLATES_PATH = path.resolve(__dirname, 'templates')

module.exports = class ComponentGenerator {
  constructor(args) {
    this.args = args

    this.fsProcessor = new FsProcessor()
  }


  get templatePath() {
    return path.resolve(TEMPLATES_PATH, `component.mustache`)
  }

  get outputPath() {
    const { name, moduleName } = this.args

    const cName = t(name)

    if(!moduleName) {
      return this.fsProcessor.resolvePath(cName)
    }

    return this.fsProcessor.resolvePath(
      cName,
      moduleName,
      'components'
    )
  }


  render() {
    const template = fs.readFileSync(this.templatePath).toString()

    

    const result = mustache.render(
      template,
      this.args
    )

    return result 
  }

  
  generate() {
    const result = this.render()

    fs.writeFileSync(this.outputPath, result)
  }
}
