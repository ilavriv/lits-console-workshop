const fs = require('fs')
const path = require('path')

const FsProcessor = require('../utils/fs-processor')

module.exports = class ModuleGenerator {
  constructor(moduleName) {
    this.moduleName = moduleName

    this.fsProcessor = new FsProcessor()
  }

  resolveConfig(configFlag) {

    if(!configFlag) {
      return require('../config/default-module.json')
    }

    const confPath = path.resolve(process.cwd(), configFlag)

    return require(confPath)
  }

  fromConfig(configFlag) {
  
    fs.mkdirSync(this.moduleName)

    const { directories, files = [] } = this.resolveConfig(configFlag)


    directories.forEach(d => {
      this.fsProcessor.createDir(d, this.moduleName)
    })

    files.forEach(f => this.fsProcessor.createFile(f, this.moduleName))
  }
}